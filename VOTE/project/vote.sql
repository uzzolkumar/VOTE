-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 23, 2016 at 08:11 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `vote`
--

-- --------------------------------------------------------

--
-- Table structure for table `candidate`
--

CREATE TABLE IF NOT EXISTS `candidate` (
`id` int(200) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `uname` varchar(20) NOT NULL,
  `passs` varchar(20) NOT NULL,
  `image` varchar(200) NOT NULL,
  `work_title` varchar(222) NOT NULL,
  `vote_count` int(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `candidate`
--

INSERT INTO `candidate` (`id`, `Name`, `uname`, `passs`, `image`, `work_title`, `vote_count`) VALUES
(1, 'Md Tahzib Ul Islam', 'Md Tahzib Ul Islam', '12345678', 'Screenshot_7.jpg', 'Asst.Professor', 1),
(2, ' Md Habibullah Belal', ' Md Habibullah Belal', '12345678', 'Screenshot_6.jpg', 'Asst.Professor', 1),
(3, 'Md Shahjahan Kabir', 'Md Shahjahan Kabir', '12345678', 's.jpg', 'Asst.Professor', 1),
(4, 'Samrat Kumar Dey', 'Samrat Kumar Dey', '12345678', 'Screenshot_5.jpg', 'Lecturer', 1);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
`login_id` int(200) NOT NULL,
  `user_name` varchar(30) NOT NULL,
  `password` varchar(20) NOT NULL,
  `rank` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`login_id`, `user_name`, `password`, `rank`) VALUES
(13, 'Uzzol kumar ', '12345678', 'student'),
(14, 'Shirim Chandro', '1234567', 'student'),
(15, 'Zakaria  Hossain', '12345678', 'student'),
(16, 'Zakaria  Hossain', '12345678', 'student'),
(17, 'Md Rayan', '12345678', 'student'),
(18, 'kumar dey', '12345678', 'student');

-- --------------------------------------------------------

--
-- Table structure for table `user_student`
--

CREATE TABLE IF NOT EXISTS `user_student` (
`id` int(20) NOT NULL,
  `FName` varchar(20) NOT NULL,
  `lname` varchar(20) NOT NULL,
  `uname` varchar(20) NOT NULL,
  `passs` varchar(20) NOT NULL,
  `course` varchar(20) NOT NULL,
  `regNO` varchar(20) NOT NULL,
  `syear` varchar(20) NOT NULL,
  `tmp` varchar(200) NOT NULL,
  `image_name` varchar(200) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_student`
--

INSERT INTO `user_student` (`id`, `FName`, `lname`, `uname`, `passs`, `course`, `regNO`, `syear`, `tmp`, `image_name`, `status`) VALUES
(112, 'Uzzol kumar ', 'chandro sheel', 'Uzzol kumar ', '12345678', 'B.sc in CSE', '91074', 'Final year', 'C:xampp	mpphp875.tmp', 'o.jpg', 'VOTED'),
(113, 'Shirim Chandro', 'Dev sharmar', 'Shirim Chandro', '1234567', 'B.sc in CSE', '91062', 'Final year', 'C:xampp	mpphpD3D4.tmp', '2.jpg', 'VOTED'),
(114, 'Zakaria ', 'Hossain', 'Zakaria  Hossain', '12345678', 'B.sc in CSE', '910075', 'Final year', 'C:xampp	mpphpC3D1.tmp', '2222.jpg', 'Not Vote'),
(115, 'Zakaria ', 'Hossain', 'Zakaria  Hossain', '12345678', 'B.sc in CSE', '910075', 'Final year', 'C:xampp	mpphp86FF.tmp', '2222.jpg', 'Not Vote'),
(116, 'Md Rayan', 'chowdury', 'Md Rayan', '12345678', 'B.sc in CSE', '910064', 'Final year', 'C:xampp	mpphp3D8A.tmp', '10246694_610014555755420_7774987109593108988_n.jpg', 'VOTED'),
(117, 'osimtu', 'kumar dey', 'kumar dey', '12345678', 'B.sc in CSE', '1234567', 'Third year', 'C:xampp	mpphp647.tmp', '12391373_1084123381618347_4497469471219693136_n.jpg', 'VOTED');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `candidate`
--
ALTER TABLE `candidate`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
 ADD PRIMARY KEY (`login_id`);

--
-- Indexes for table `user_student`
--
ALTER TABLE `user_student`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `candidate`
--
ALTER TABLE `candidate`
MODIFY `id` int(200) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
MODIFY `login_id` int(200) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `user_student`
--
ALTER TABLE `user_student`
MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=118;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
